/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z808;

import java.util.Vector;

/**
 *
 * @author Weslen
 */
public class Memoria {

    int[] memoria;
    int[] tabelaDeSimbolos;

    Memoria() {
        this.memoria = new int[500];
        this.tabelaDeSimbolos = new int[80];

    }

    int getMemoria(int pos) {
        int posicaoReal = this.tabelaDeSimbolos[pos];
        this.getPosTabelaSimbolos(posicaoReal);
        return this.memoria[pos];
    }

    void setMemoria(int valor, int pos) {
        this.tabelaDeSimbolos[pos] = valor;
        this.memoria[pos] = valor;

    }

    int getPosTabelaSimbolos(int pos) {
        if (this.tabelaDeSimbolos[pos] == 0) {
            return pos;
        } else {
            for (int x = 0; x < this.tabelaDeSimbolos.length; x++) {
                if (this.tabelaDeSimbolos[x] != 0) {
                    return x;
                }
            }
        }
        return 0;
    }

}
